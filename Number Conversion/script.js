var decimal =document.getElementById("decimal");
var binary = document.getElementById("binary");
var octal = document.getElementById("octal");
var hexa = document.getElementById("hexa");
function decTo(){
	binary.value = parseInt(decimal.value).toString(2);
	octal.value = parseInt(decimal.value).toString(8);
	hexa.value = parseInt(decimal.value).toString(16).toUpperCase();
}
function binaryTo(){
	decimal.value = parseInt(binary.value, 2);
	octal.value = parseInt(binary.value, 2).toString(8);
	hexa.value = parseInt(binary.value, 2).toString(16).toUpperCase();
}
function octalTo(){
	decimal.value = parseInt(octal.value,8);
	binary.value = parseInt(octal.value,8).toString(2);
	hexa.value = parseInt(octal.value,8).toString(16).toUpperCase();
}
function hexaTo(){
	decimal.value = parseInt(hexa.value,16);
	binary.value = parseInt(hexa.value,16).toString(2);
	octal.value = parseInt(hexa.value,16).toString(8);
}